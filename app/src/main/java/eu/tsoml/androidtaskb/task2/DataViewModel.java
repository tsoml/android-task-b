package eu.tsoml.androidtaskb.task2;

import android.telephony.PhoneNumberUtils;
import android.util.Patterns;


class DataViewModel {

    DataViewModel() {
    }

    boolean isUserNameValid(String username) {
        return !username.equals("");
    }

    boolean isPhoneValid(String phone) {
        return PhoneNumberUtils.isGlobalPhoneNumber(phone);
    }

    boolean isEmailValid(String email) {
        if (email.equals("")) {
            return false;
        }
        if (email.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        } else {
            return !email.trim().isEmpty();
        }
    }

    boolean isPasswordValid(String password) {
        return !password.equals("") && password.trim().length() > 5;
    }

    boolean isPasswordConfirmValid(String password, String passwordConfirm) {
        return passwordConfirm.equals(password);
    }

}

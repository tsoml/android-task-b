package eu.tsoml.androidtaskb.task2;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import eu.tsoml.androidtaskb.R;
import eu.tsoml.androidtaskb.databinding.ActivityTask2Binding;


public class Task2Activity extends AppCompatActivity {

    private ActivityTask2Binding binding;
    private DataViewModel dataViewModel = new DataViewModel();

    private String userName;
    private String email;
    private String phone;
    private String password;
    private String password_confirm;

    private Boolean isValid = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTask2Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userName = binding.etUsername.getText().toString();
                email = binding.etEmail.getText().toString();
                phone = binding.etPhone.getText().toString();
                password = binding.etPassword.getText().toString();
                password_confirm = binding.etPasswordConfirm.getText().toString();

                if (!dataViewModel.isUserNameValid(userName)) {
                    binding.etUsername.setError(getString(R.string.invalid_username));
                    isValid = false;
                }  else {
                    isValid = true;
                }

                if (!dataViewModel.isPasswordValid(password)) {
                    binding.etPassword.setError(getString(R.string.invalid_password));
                    isValid = false;
                }  else {
                    isValid = true;
                }
                if (!dataViewModel.isEmailValid(email)) {
                    binding.etEmail.setError(getString(R.string.invalid_email));
                    isValid = false;
                }  else {
                    isValid = true;
                }

                if (!dataViewModel.isPhoneValid(phone)) {
                    binding.etPhone.setError(getString(R.string.invalid_phone));
                    isValid = false;
                }  else {
                    isValid = true;
                }

                if (!dataViewModel.isPasswordConfirmValid(password, password_confirm)) {
                    binding.etPasswordConfirm.setError(getString(R.string.invalid_password_confirm));
                    isValid = false;
                }  else {
                    isValid = true;
                }

                if (isValid) {
                    Toast.makeText(Task2Activity.this, "Validation Complete", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

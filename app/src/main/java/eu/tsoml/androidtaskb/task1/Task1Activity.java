package eu.tsoml.androidtaskb.task1;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import eu.tsoml.androidtaskb.databinding.ActivityTask1Binding;

public class Task1Activity extends AppCompatActivity {

    private ActivityTask1Binding binding;
    private ArrayList<String> arrayList = new ArrayList<String>();
    private String currentString;
    private int currentIndex = 0;
    private int arrayLimit = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityTask1Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.editText.getText().toString().equals("")) {
                    currentString = binding.editText.getText().toString();
                    if (currentIndex < arrayLimit) {
                        arrayList.add(currentString);
                        currentIndex++;
                        Toast.makeText(Task1Activity.this, "" + currentIndex, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Task1Activity.this, "Error: data is full", Toast.LENGTH_SHORT).show();
                    }
                    binding.editText.setText("");
                } else {
                    Toast.makeText(Task1Activity.this, "Error: data is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });


        binding.btnRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentIndex >= 0) {
                    binding.editText.setText(arrayList.get(currentIndex - 1));
                    Toast.makeText(Task1Activity.this, arrayList.get(currentIndex - 1), Toast.LENGTH_SHORT).show();
                    currentIndex--;
                } else {
                    Toast.makeText(Task1Activity.this, "Error: data is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}

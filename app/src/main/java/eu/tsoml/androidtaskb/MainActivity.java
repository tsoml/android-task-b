package eu.tsoml.androidtaskb;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import eu.tsoml.androidtaskb.databinding.ActivityMainBinding;
import eu.tsoml.androidtaskb.task1.Task1Activity;
import eu.tsoml.androidtaskb.task2.Task2Activity;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.btnTask1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task1Activity.class);
                startActivity(intent);
            }
        });

        binding.btnTask2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task2Activity.class);
                startActivity(intent);
            }
        });
    }
}
